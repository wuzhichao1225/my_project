'''
输出网易云我最喜欢的音乐
'''
from selenium import webdriver
from selenium.webdriver.common.by import By
from time import sleep
import pandas as pd
import xlwt
#加载浏览器驱动
driver = webdriver.Chrome()
driver.get("https://music.163.com/")
driver.maximize_window()
sleep(2)
driver.find_element(By.XPATH,'//a[text()="登录"]').click()
sleep(2)
driver.find_element(By.XPATH,'/html/body/div[7]/div/div[2]/div/div[2]/div/div/div/a').click()
#勾选服务条款
driver.find_element(By.XPATH,'//*[@id="j-official-terms"]').click()
#点击手机号登录
driver.find_element(By.XPATH,'/html/body/div[7]/div/div[2]/div/div[2]/div/div/div/div[1]/div[1]/a[1]/div').click()
#切换到短信登录
driver.find_element(By.XPATH,'//a[text()="密码登录"]').click()
#输入手机号和密码
driver.find_element(By.XPATH,'/html/body/div[7]/div/div[2]/div/div[2]/section/div[1]/div/input').send_keys("17324776320")
driver.find_element(By.XPATH,'/html/body/div[7]/div/div[2]/div/div[2]/section/div[2]/div/input').send_keys("wzc122500wzc")
#点击登录
driver.find_element(By.XPATH,'/html/body/div[7]/div/div[2]/div/div[2]/section/a/div').click()
#切换iframe
#driver.switch_to.frame("g_iframe")
#点击排行榜
driver.find_element(By.XPATH,'//*[@id="g_nav2"]/div/ul/li[2]/a/em').click()
#切换iframe
#driver.switch_to.frame("g_iframe")
#点击分享
#driver.find_element(By.XPATH,'//*[@id="toplist-share"]/i').click()
#点击我的音乐
driver.find_element(By.XPATH,'//*[@id="g-topbar"]/div[1]/div/ul/li[2]/span/a/em').click()
#切换iframe
driver.switch_to.frame("g_iframe")
#定位我喜欢的音乐

sleep(3)
list = []
txts = driver.find_elements(By.XPATH,'//*[@class="even "]/td[2]/div/div/div/span/a/b')
for i in txts:
    list.append(i.get_attribute('title'))
print(list)
#创建一个表格文件
book = xlwt.Workbook(encoding='utf-8',style_compression=0)
# #创建一个sheet页
# sheet = book.add_sheet('我喜欢的音乐',cell_overwrite_ok=True)
# col = ('音乐名')
# #写入列名 write方法，该方法的第一个参数是行、第二个参数是列、第三个当然就是col元组值
# for i in range(0,1):
# 		sheet.write(0,i,col[i])
# #获取list长度
# l = len(list)
# #数据写入表单
# for i in range(0,l):
# 		data = list[i]
# 		for j in range(0,1):
# 			sheet.write(i+1,j,data[j])
# #写入表单
# savepath = 'D:/test/输出测试.xls'
# book.save(savepath)





